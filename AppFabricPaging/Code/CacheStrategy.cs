﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.ApplicationServer.Caching;

namespace AppFabricPaging.Code
{
    public static class CacheStrategy
    {
        private static DataCacheFactory _cacheFactory = null;
        private static DataCache _cache = null;

        private static string _cacheServer;
        private static string _cacheName;
        private static int _cachePort;

        private static readonly int _cachePageSize = 100;

        public static int CachePageSize
        {
            get
            {
                return _cachePageSize;
            }
        }

        public static DataCache GetCache()
        {
            if (_cache != null)
                return _cache;

            _cacheServer = ConfigurationManager.AppSettings["CacheServer"];
            _cacheName = ConfigurationManager.AppSettings["CacheName"];
            _cachePort = int.Parse(ConfigurationManager.AppSettings["CachePort"]);

            var cacheServers = new List<DataCacheServerEndpoint>
            {
                new DataCacheServerEndpoint(_cacheServer, _cachePort)
            };

            var config = new DataCacheFactoryConfiguration
            {
                Servers = cacheServers.ToArray()
            };

            //increase to max to handle large data sets
            config.TransportProperties.MaxBufferSize = 2147483647;
            config.TransportProperties.MaxBufferPoolSize = 2147483647;
            config.TransportProperties.ReceiveTimeout = new TimeSpan(0,0,0,900000,0);

            _cacheFactory = new DataCacheFactory(config);

            _cache = _cacheFactory.GetCache(_cacheName);

            return _cache;
        }
    }
}