﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppFabricPaging.Code
{
    public class DataStrategy
    {
        public IEnumerable<Test> GetAll()
        {
            List<Test> items;

            using (var db = new TestDbEntities())
            {
                items = db.Test.ToList();
            }

            return items;
        }
    }
}