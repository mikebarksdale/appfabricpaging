﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace AppFabricPaging.Code
{
    public interface ICacheableModel
    {
        string GetCacheKey();
    }

    public class SearchModel : ICacheableModel
    {
        public string SearchText { get; set; }
        public string ApplicationName { get; set; }
        public int PageSize { get; set; }

        public string GetCacheKey()
        {
            return string.Format("{0}{1}{2}{3}", SearchText, ApplicationName, PageSize, DateTime.Now.ToShortDateString());
        }
    }

    [Serializable]
    public class SearchResult
    {
        public DateTime RecordDate { get; set; }
        public string Id { get; set; }
        public int Value { get; set; }
    }

    public class SearchStrategy
    {
        public IEnumerable<SearchResult> Search(SearchModel searchModel, int page = 0, int pageSize = 10)
        {
            //check the cache first
            var results = GetPagedResultsFromCache(searchModel, page, pageSize);

            //found in the cache
            if (results != null)
                return results.Skip((page % pageSize) * pageSize).Take(pageSize);
            
            //get from the db since nothing was found in the cache
            results = new DataStrategy().GetAll().Select(i => new SearchResult
                {
                    Id = i.UniqueCol.ToString(),
                    Value = i.Value.HasValue ? i.Value.Value : 0,
                    RecordDate = DateTime.Now
                }).OrderBy(i => i.Value);

            //place the results in the cache on a separate thread
            Task.Factory.StartNew(() => AddResultsToCache(searchModel, results.ToList()));

            return results;
        }

        private IEnumerable<SearchResult> GetPagedResultsFromCache(SearchModel searchModel, int page, int pageSize)
        {
            return (IEnumerable<SearchResult>)CacheStrategy.GetCache().Get(
                string.Format("{0}_Page{1}",
                              searchModel.GetCacheKey(),
                              GetCachePageNumber(searchModel.GetCacheKey(), page, pageSize, CacheStrategy.CachePageSize)));
        }

        private void AddResultsToCache(SearchModel searchModel, List<SearchResult> results)
        {
            for (int x = 0; x < results.Count() / CacheStrategy.CachePageSize; x++)
            {
                CacheStrategy.GetCache().Put(string.Format("{0}_Page{1}", searchModel.GetCacheKey(), x + 1),
                                             results.Skip(x * CacheStrategy.CachePageSize).Take(CacheStrategy.CachePageSize).ToList());
            }
        }

        private string GetCachePageNumber(string cacheKey, int page, int pageSize, int cachePageSize)
        {
            int cachePageNumber = ((page * pageSize) / CacheStrategy.CachePageSize) + 1;

            return cachePageNumber.ToString();
        }
    }
}