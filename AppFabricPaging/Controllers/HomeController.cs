﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AppFabricPaging.Code;

namespace AppFabricPaging.Controllers
{
    public class HomeController : Controller
    {
        private SearchStrategy _strategy;

        public HomeController()
        {
            _strategy = new SearchStrategy();
        }

        public ActionResult Index()
        {
            var start = DateTime.Now;
            _strategy.Search(new SearchModel
                {
                    ApplicationName = "TestApplication",
                    SearchText = "Query",
                    PageSize = 50
                });
            var end = DateTime.Now;

            ViewBag.SearchTime = (end - start).TotalMilliseconds;

            return View();
        }

        public ActionResult GetNextPage(int pageNumber)
        {
            return PartialView("SearchResultView", _strategy.Search(new SearchModel
            {
                ApplicationName = "TestApplication",
                SearchText = "Query",
                PageSize = 50
            }, pageNumber, 10));
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
